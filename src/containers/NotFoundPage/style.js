import styled from 'styled-components';
import BACKGROUND from 'assets/backgroundDesktop.jpg';
import BACKGROUNDMOBILE from 'assets/backgroundMobile.jpg';

export const NotFoundPageStyle = styled.div`
    .nonContainer{
        width:100%;
        min-height:100vh;
        display:flex;
        justify-content:center;
        align-items:center;
        background-image:url(${BACKGROUND});
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;

        h1{

            .sorry{
                color:${props => props.theme.colors.darkBlue};
            }

            .comma{
                color:${props => props.theme.colors.yellow};
            }

            .notFound{
                color:${props => props.theme.colors.blue};
            }
        }
    }

    @media(max-width:728px){
        .nonContainer{
            background-image:url(${BACKGROUNDMOBILE});

            h1{
                font-size:1.25rem;
            }
        }
    }


`
