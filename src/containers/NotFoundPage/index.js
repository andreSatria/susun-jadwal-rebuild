import React from 'react';
import { NotFoundPageStyle } from './style';


export default class NotFoundPage extends React.Component {
  render() {
    return (
        <NotFoundPageStyle>
            <div className="nonContainer">
                <h1><span className="sorry">Sorry</span><span className="comma"> , </span><span className="notFound"> Page Not Found</span> </h1>
            </div>
        </NotFoundPageStyle>
    );
  }
}
