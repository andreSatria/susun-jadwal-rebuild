/* eslint-disable lines-around-comment, no-plusplus, no-param-reassign, array-callback-return, consistent-return */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { LoginStyle } from './style';
import { setLoginData, authLogin } from './actions';
import SUSUNJADWALPIC from 'assets/logoSusun.png';

import { getCookie } from 'utils';

class LoginPage extends React.Component {
  componentDidMount() {
    window.processLogin = (data) => {
      this.setCookie(data);
      this.props.push('/susun');
    };

    const major_id = getCookie("major_id");
    const token = getCookie("token");
    const user_id = getCookie("user_id");
    if (major_id !== '' && token !== '' && user_id !== '') {
      this.props.setLoginData(major_id, token, user_id);
      this.props.push('/susun');
    }
  }

  setCookie(data) {
    /* set cookies */
    const date = new Date();
    date.setTime(date.getTime() + (30 * 24 * 60 * 60 * 1000));
    const expires = `expires=${date.toUTCString()}`;
    document.cookie = `major_id=${data.major_id}; expires=${expires};path=/`;
    document.cookie = `token=${data.token}; expires=${expires};path=/`;
    document.cookie = `user_id=${data.user_id}; expires=${expires};path=/`;
    this.props.setLoginData(data.major_id, data.token, data.user_id);
  }

  ssoLogin = () => {
    window.open('http://localhost:9000/api', 'Sunjad_SSO_Login', 'width=600,height=600');
    // const dummy_user = {
    //   token: "8VlGnna26REH6xrh",
    //   npm: "1606895462",
    //   name: "William Rumanta",
    //   role: "user",
    //   major: "ilmu-komputer",
    //   angkatan: "2016"
    // };

    // this.props.authLogin(dummy_user);
  }

  render() {
    return (
      <LoginStyle>
        <div className="wholeContainer">
          <div className="loginContainer">
            <img src={SUSUNJADWALPIC} alt="susunPict" />
            <button className="loginButton" onClick={this.ssoLogin}>login with SSO</button>
          </div>
        </div>
      </LoginStyle>
    );
  }
}

LoginPage.propTypes = {
  authLogin: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  setLoginData: PropTypes.func.isRequired,
}

function mapDispatchToProps(dispatch) {
  return {
    authLogin: (user) => dispatch(authLogin(user)),
    dispatch,
    push: (url) => (dispatch(push(url))),
    setLoginData: (majorId, token, userId) => dispatch(setLoginData(majorId, token, userId)),
  };
}

export default connect(null, mapDispatchToProps)(LoginPage);