/* eslint-disable sort-keys, newline-per-chained-call */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  SET_LOGIN_DATA,
  LOG_OUT,
  LOADING,
  LOADING_DONE,
  AUTH_LOGIN,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILED,
} from './constants';

const initialState = fromJS({
  major_id: '',
  token: '',
  user_id: '',
  loading: false,
  error: '',
});

function loginPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SET_LOGIN_DATA:
      return state.set('major_id', action.majorId).set('token', action.token).set('user_id', action.userId);
    case LOG_OUT:
      return state.set('major_id', '').set('token', '').set('user_id', '');
    case LOADING:
      return state.set('loading', true);
    case LOADING_DONE:
      return state.set('loading', false);
    case AUTH_LOGIN:
      return state.set('loading', true);
    case AUTH_LOGIN_SUCCESS:
      return state.set('major_id', action.payload.major_id).set('token', action.payload.token).set('user_id', action.payload.user_id).set('loading', false);
    case AUTH_LOGIN_FAILED:
      return state.set('error', action.payload)
      default:
      return state;
  }
}

export default loginPageReducer;
