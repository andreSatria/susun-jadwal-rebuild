/* eslint-disable sort-keys */
import axios from 'axios';
import { push } from 'react-router-redux';
import { authLoginApi } from 'api';
import { setCookie } from 'utils';

import {
  SET_LOGIN_DATA,
  LOG_OUT,
  LOADING,
  LOADING_DONE,
  AUTH_LOGIN,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILED,
} from './constants';

export function authLogin(user) {
  return (dispatch) => {
    dispatch({
      type: AUTH_LOGIN
    })

    axios.post(authLoginApi, user)
      .then((response) => {
        dispatch({
          type: AUTH_LOGIN_SUCCESS,
          payload: response.data
        })

        setCookie(response.data);

        dispatch(push('/susun'));
      })
      .catch((error) => {
        dispatch({
          type: AUTH_LOGIN_FAILED,
          payload: error.response
        })
      })
  }
}

export function setLoginData(majorId, token, userId) {
  return {
    type: SET_LOGIN_DATA,
    userId,
    majorId,
    token,
  };
}

export function logOut() {
  return {
    type: LOG_OUT,
  };
}

export function loading() {
  return {
    type: LOADING,
  };
}

export function loadingDone() {
  return {
    type: LOADING_DONE,
  };
}