/*
 *
 * Login constants
 *
 */
export const DEFAULT_ACTION = 'src/LoginPage/DEFAULT_ACTION';
export const SET_LOGIN_DATA = 'src/LoginPage/SET_LOGIN_DATA';
export const LOG_OUT = 'src/LoginPage/LOG_OUT';
export const LOADING = 'src/LoginPage/LOADING';
export const LOADING_DONE = 'src/LoginPage/LOADING_DONE';
export const AUTH_LOGIN = 'src/LoginPage/AUTH_LOGIN';
export const AUTH_LOGIN_SUCCESS = 'src/LoginPage/AUTH_LOGIN_SUCCESS';
export const AUTH_LOGIN_FAILED = 'src/LoginPage/AUTH_LOGIN_FAILED';

