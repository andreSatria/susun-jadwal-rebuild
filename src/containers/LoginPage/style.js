import styled from 'styled-components';
import BACKGROUND from 'assets/backgroundDesktop.jpg'
import BACKGROUNDMOBILE from 'assets/backgroundMobile.jpg'

export const LoginStyle = styled.div`
  width:100%;
  min-height:100vh;
  padding:0;
  overflow:hidden;

  .wholeContainer {
    width:100%;
    height:100%;
    min-height:100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-image:url(${BACKGROUND});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;

    .loginContainer {
      width: 100%;
      height: 100%;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;

      img{
        height: 20rem;
      }

      .loginButton {
        width:28rem;
        height:3rem;
        margin-top:-3rem;
        border-radius:0.5rem;
        border: none;
        box-shadow: 6px 10px 5px 0px rgba(0,0,0,0.1);
        background:linear-gradient(to right, #245472 0% ,#3399cc 51%, #cccccc 125%);
        font-size:1.25rem;
        color:white;
        transition: all 0.3s ease 0s;
        cursor:pointer;

        &:hover {
          box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.5);
          transform: translateY(-7px);
        }

      }

    }

  }

  @media (max-width:768px){
    .wholeContainer {
      background-image:url(${BACKGROUNDMOBILE});

      .loginContainer {
        img {
          width:100%;
          height:auto;
        }

        .loginButton {
          width:45%;
          height:1.5rem;
          font-size:0.75rem;
          margin-top:-1.5rem;
        }
      }
    }
  }
`;
