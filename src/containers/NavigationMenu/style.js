import styled from 'styled-components';


export const NavBar = styled.div`

.navbar{
  overflow:hidden;
  height:3.75rem;
  width:100%;
  justify-content:space-between;
  align-items:space-evenly;
  display:flex;
  background:none;


  .menuButton{
    flex:1;
    background:transparent;
    border:none;
    display:flex;
    align-items:flex-start;

    img{
      width:1.75rem;
      margin-left:1rem;
      height:auto;
    }
  }

  .logoContainer{
    flex:2;
    display:flex;
    justify-content: center;
    align-items: center;

    img{
      width:15rem;
    }
  }

  .searchContainer{
    display:flex;
    flex:1;
    justify-content: flex-end;
    align-items: center;


    .searchBar{
      width:60%;
      height:2rem;
      border-radius:5em;
      font-size:0.75rem;
      padding: 0.5em 0em 0.5em 1.25em;
      border: solid 2px #cecfcf;
      box-sizing: border-box;
      background: transparent;

    }

    .searchButton{
      width:3rem;
      height:4rem;
      background:transparent;
      border:none;

      img{
        width:2.25rem;
        height:auto;
        background-size:contain;

      }
    }

  }

  @media (max-width: 64em){

    .menuButton{
      img{
        width:1.5rem;
      }
    }
    .logoContainer{
      img{
        width:10rem;
      }
    }

    .searchContainer{
      .searchBar{
        display:none;
      }

      .searchButton {
        img{
          width:1.75rem;
        }
      }

    }

  }

}






`;
