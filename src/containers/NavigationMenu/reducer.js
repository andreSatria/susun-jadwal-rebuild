import { fromJS } from 'immutable';
import {
  ADD_GOOGLE_CALENDAR,
  ADD_GOOGLE_CALENDAR_SUCCESS,
  ADD_GOOGLE_CALENDAR_FAILED,
  DECIDER,
  SEARCH,
  SUBMIT_JADWAL,
  SUBMIT_JADWAL_SUKSES,
} from './constants';


const initialState = fromJS({
  error: '',
  google: null,
  isLoading: false,
  menuDecider: null,
  pickedCourses: [],
  query: "",
});


function searchReducer(state = initialState, action) {
  switch (action.type) {

    case SEARCH:
      return state.set('query', action.payload)
    case DECIDER:
      return state.set('menuDecider', action.payload)
    case SUBMIT_JADWAL:
      return state.set('isLoading', true)
    case SUBMIT_JADWAL_SUKSES:
      return state.set('pickedCourses', action.payload).set('isLoading', false)
    case ADD_GOOGLE_CALENDAR:
      return state.set('isLoading', true)
    case ADD_GOOGLE_CALENDAR_SUCCESS:
      return state.set('google', action.payload).set('isLoading', false)
    case ADD_GOOGLE_CALENDAR_FAILED:
      return state.set('error', action.payload)

    default:
      return state;
  }
}

export default searchReducer;
