import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
// import {bindActionCreators} from 'redux';
import NavBar from 'components/NavBar';
import SideMenu from 'components/SideMenu';
import DarkenBackground from 'components/DarkenBackground';
import AddAgendaForm from 'components/AddAgendaForm';
import { fetchPickedCourses } from 'containers/ShowSchedulePage/actions';
import { fetchCourses, addClassSchedule, removeClassSchedule, addAgendaSchedule } from 'containers/PickSchedulePage/actions';
import {
  getCourses,
  isLoadingCourses,
  isLoadedCourses,
  getAddedClasses,
  getPickedCourses,
  getQuery,
  getMenu,
  getUser,
  getActiveJadwal,
} from 'selectors';
import { getCookie } from 'utils';
import { setLoginData } from '../LoginPage/actions';
import { search, menuDecider, saveJadwal, addGoogleCalendar } from "./actions";

class NavigationMenu extends Component {
    constructor() {
        super();
        this.state = {
            addAgendaModal: false,
            live: false,
            menuOpen: false,
            toggled: false,
            width: null,
        }
        this.handleClickAddAgenda = this.handleClickAddAgenda.bind(this)
    }


    UNSAFE_componentWillMount() {
        this.resize();
    }


    componentDidMount() {
      const major_id = getCookie('major_id');
      const token = getCookie('token');
      const user_id = getCookie('user_id');
      if (!(major_id !== '') || !(token !== '') || !(user_id !== '')) {
        this.props.push('/');
      } else {
        if (this.props.user.major_id === '' && this.props.user.token === '' && this.props.user.user_id === '') {
          this.props.setLoginData(major_id, token, user_id);
        }
      }

      if (!this.props.isLoadedCourses) {
        this.props.fetchCourses();
        this.props.fetchPickedCourses('', false);
        this.props.search();
        window.addEventListener('resize', this.resize.bind(this));
      }
    }

    resize =() => {
        this.setState({ width: window.innerWidth <= 768 });
    }


    menuToggler = () => {
        this.setState((prev) => (
            { menuOpen: !prev.menuOpen }
        ));
    }


    liveToggler = () => {
        this.setState({live: true});
    }

    defaultToggler = () => {
        this.setState({live: false});
    }

    convertToMinute = (time) => {
        return time.substring(0, 2) * 60 + Number(time.substring(3, 5))
    }

    checkTime = (jadwalEnd, jadwalStart, jadwal2Start, jadwal2End) => {
        const start = this.convertToMinute(jadwalStart)
        const end = this.convertToMinute(jadwalEnd)
        const nextStart = this.convertToMinute(jadwal2Start)
        if (start <= nextStart && end >= nextStart) {
            return true;
        }

        return false
    }

    checkConflict = (data) => {
        const filteredData = Array(data.length).fill(false)
        if (data.length > 1) {
            for (let i = 0; i < data.length; i++) {
                for (let j = i+1; j < data.length; j++) {
                    for (let k = 0;k < data[i].class.jadwal.length;k++) {
                        for (let m = 0; m < data[j].class.jadwal.length;m++) {
                            if(data[i].class.jadwal[k].day === data[j].class.jadwal[m].day) {
                                let smaller;
                                let larger;
                                let bool;
                                if (data[i].class.jadwal[k].start >= data[j].class.jadwal[m].start) {
                                    smaller = data[j].class;
                                    larger = data[i].class;
                                } else {
                                    smaller = data[i].class;
                                    larger = data[j].class;
                                }

                                if (smaller === data[i].class) {
                                    bool = this.checkTime(smaller.jadwal[k].end, smaller.jadwal[k].start, larger.jadwal[m].start)
                                } else {
                                    bool = this.checkTime(smaller.jadwal[m].end, smaller.jadwal[m].start, larger.jadwal[k].start)
                                }

                                if (bool) {
                                    filteredData[i] = true;
                                    filteredData[j] = true;
                                }
                            }
                        }
                    }

                }
            }
        }

        return filteredData;
    }

    countSks = (data) => {
        let count = 0;

        for (let i = 0; i < data.length;i++) {
            count += parseInt(data[i].sks)
        }

        return count;
    }

    pickClass = (classSchedule, agenda = false) => {
      if (agenda) {
        this.props.addAgendaSchedule(classSchedule);
      }
      this.props.removeClassSchedule(classSchedule);
      this.props.addClassSchedule(classSchedule, agenda);
    }


    searchFunction = (matkul) => {
        if (!matkul) return this.props.course;

        let query = !this.props.query ? "" : this.props.query;
        query = query.replace(/\\/g, "\\\\");
        const filteredCourse = matkul.slice().filter((matkulName) => {
            return matkulName.name.toLowerCase().match(query.toLowerCase());
        })

        return filteredCourse;
    }

    print = (some) => {
        this.props.search(some)

        return this.setState({"query": some})
    }

    handleClickAddAgenda = (bool) => {
        this.setState({
            addAgendaModal: bool
        })
    }

    handleClickPindahPage = (endpoint) => {
        this.setState({
            menuOpen: false
        });
        this.props.push(endpoint);
    }

    handleClickSimpanJadwal = (courses, pindah = false) => {
      this.props.saveJadwal(courses, pindah);
    }

    handleClickGoogleCalendar = () => {
        this.props.addGoogleCalendar(this.props.activeJadwal.id);
    }

render() {
    const state = this.state;
    const decider = this.checkConflict(this.props.addedClasses);
    const addScheduleForm = state.addAgendaModal ?
                                <AddAgendaForm
                                    onClickAddAgenda={this.handleClickAddAgenda}
                                    mobile={this.state.width}
                                    addAgenda={this.pickClass}
                                />
                                : null;

    const sideMenu = <SideMenu show = {this.state.menuOpen}
                        menuToggler = {this.menuToggler}
                        indicator = {this.state.live}
                        liveToggler = {this.liveToggler}
                        defaultToggler = {this.defaultToggler}
                        sideFactory = {this.sideFactory}
                        data={this.props.addedClasses}
                        courses={this.searchFunction(this.props.courses)}
                        remove={this.props.removeClassSchedule}
                        decider={decider}
                        indicate={decider.includes(true)}
                        count = {this.countSks(this.props.addedClasses)}
                        search={this.props.search}
                        query={this.props.query}
                        sideDecider ={this.props.sideDecider}
                        pickClass={this.pickClass}
                        onClickAddAgenda={this.handleClickAddAgenda}
                        onClickPindahPage={this.handleClickPindahPage}
                        onClickSimpanJadwal={this.handleClickSimpanJadwal}
                        pickedCourses={this.props.pickedCourses}
                        onClickGoogleCalendar={this.handleClickGoogleCalendar}
                        />;
    let darkenBg;
    const menuOpen = state.menuOpen;
    // this.sideCardFactory(state.data)


    if (menuOpen) {
        darkenBg = <DarkenBackground menuToggler = {this.menuToggler}/>
    }


    return (
      <div>
          <NavBar menuToggler={this.menuToggler} search={this.props.search} mobile={this.state.width} sideDecider={this.props.sideDecider}></NavBar>
          { sideMenu }
          { darkenBg }
          { addScheduleForm }
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    activeJadwal: getActiveJadwal(state),
    addedClasses: getAddedClasses(state),
    courses: getCourses(state),
    isLoadedCourses: isLoadedCourses(state),
    isLoadingCourses: isLoadingCourses(state),
    pickedCourses: getPickedCourses(state),
    query: getQuery(state),
    sideDecider: getMenu(state),
    user: getUser(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addAgendaSchedule: (classSchedule) => dispatch(addAgendaSchedule(classSchedule)),
    addClassSchedule: (classSchedule) => dispatch(addClassSchedule(classSchedule)),
    addGoogleCalendar: (jadwalId) => dispatch(addGoogleCalendar(jadwalId)),
    fetchCourses: () => dispatch(fetchCourses()),
    fetchPickedCourses: (param, isJadwal) => dispatch(fetchPickedCourses(param, isJadwal)),
    indicateMenu: (bool) => dispatch(menuDecider(bool)),
    push: (url) => dispatch(push(url)),
    removeClassSchedule: (classSchedule) => dispatch(removeClassSchedule(classSchedule)),
    saveJadwal: (courses, pindah) => dispatch(saveJadwal(courses, pindah)),
    search: (query) => dispatch(search(query)),
    setLoginData: (majorId, token, userId) => dispatch(setLoginData(majorId, token, userId)),
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(NavigationMenu);
