export const SEARCH = 'src/NavigationMenu/SEARCH';
export const DECIDER = 'src/NavigationMenu/DECIDER';
export const SUBMIT_JADWAL = 'src/NavigationMenu/SUBMIT_JADWAL';
export const SUBMIT_JADWAL_SUKSES = 'src/NavigationMenu/SUBMIT_JADWAL_SUKSES';
export const SUBMIT_JADWAL_GAGAL = 'src/NavigationMenu/SUBMIT_JADWAL_GAGAL';
export const ADD_GOOGLE_CALENDAR = 'src/NavigationMenu/ADD_GOOGLE_CALENDAR';
export const ADD_GOOGLE_CALENDAR_SUCCESS = 'src/NavigationMenu/ADD_GOOGLE_CALENDAR_SUCCESS';
export const ADD_GOOGLE_CALENDAR_FAILED = 'src/NavigationMenu/ADD_GOOGLE_CALENDAR_FAILED';