
import axios from 'axios';
import swal from 'sweetalert';
import { push } from 'react-router-redux';

import { getAuthUser } from 'selectors';

import {
  saveJadwalApi,
  setJadwalUtamaApi,
  addGoogleCalendarApi,
} from 'api';

import {
  SEARCH,
  DECIDER,
  SUBMIT_JADWAL,
  SUBMIT_JADWAL_SUKSES,
  SUBMIT_JADWAL_GAGAL,
  ADD_GOOGLE_CALENDAR,
  ADD_GOOGLE_CALENDAR_SUCCESS,
  ADD_GOOGLE_CALENDAR_FAILED,
} from './constants';

import {
FETCH_PICKED_COURSES_SUCCESS
} from '../ShowSchedulePage/constants';

const user = getAuthUser();

export function search(query) {
  return (dispatch) => {
    dispatch({
      payload: query,
      type: SEARCH,
    });
  }
}

export function menuDecider(bool) {
  return (dispatch) => {
    dispatch({
      payload: bool,
      type: DECIDER,
    });
  }
}

export function addGoogleCalendar(jadwalId) {
  return (dispatch) => {
    dispatch({
      type: ADD_GOOGLE_CALENDAR
    })

    axios.get(addGoogleCalendarApi(user.user_id, jadwalId),
    {
      headers: {
      'Authorization': `JWT ${user.token}`,
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',

    },
    })
      .then((response) => {
        dispatch({
          payload: response.data,
          type: ADD_GOOGLE_CALENDAR_SUCCESS,
        })
        swal("Submit Success!", "Berhasil menambahkan jadwal ke Google Calendar!", "success");
      })

      .catch((error) => {
        dispatch({
          payload: error.response,
          type: ADD_GOOGLE_CALENDAR_FAILED,
        })
        swal("Submit Failed!", "Gagal menambahkan jadwal ke Google Calendar!", "error");
      })
  }
}

export function saveJadwal(courses, pindah) {

  return (dispatch) => {
    dispatch({
      type: SUBMIT_JADWAL
    })
    const finalSchedule = [];

    courses.map((course) => {
      finalSchedule.push(
        {
          name: course.class.name,
          day: course.class.jadwal[0].day,
          start: course.class.jadwal[0].start,
          end: course.class.jadwal[0].end,
          room: course.class.jadwal[0].room
        }
      )
      if (course.class.jadwal.length > 1) {
        finalSchedule.push(
          {
            name: course.class.name,
            day: course.class.jadwal[1].day,
            start: course.class.jadwal[1].start,
            end: course.class.jadwal[1].end,
            room: course.class.jadwal[1].room
          }
        )
      }
      if (course.class.jadwal.length > 2) {
        finalSchedule.push(
          {
            name: course.class.name,
            day: course.class.jadwal[2].day,
            start: course.class.jadwal[2].start,
            end: course.class.jadwal[2].end,
            room: course.class.jadwal[2].room
          }
        )
      }
    })

    const data = {
      jadwals: finalSchedule
    }

    axios.post(saveJadwalApi(user.user_id), data)
      .then((response) => {
        dispatch({
          payload: response.data,
          type: SUBMIT_JADWAL_SUKSES,
        })
        const jadwalId = response.data.jadwal_id;

        axios.post(setJadwalUtamaApi(user.user_id, jadwalId))
          .then(() => {
            swal("Submit Success!", "Jadwal telah disimpan!", "success");
            dispatch({
              payload: response.data,
              type: FETCH_PICKED_COURSES_SUCCESS,
            })
            if (pindah) {
              dispatch(push(`/jadwal/${jadwalId}`))
            }
          })
          .catch((err) => {
            dispatch({
              payload: err.response,
              type: SUBMIT_JADWAL_GAGAL,
            })
            swal("Submit Failed!", "Jadwal gagal disimpan!", "error");
          })
      })
      .catch((error) => {
        dispatch({
          payload: error.response,
          type: SUBMIT_JADWAL_GAGAL,
        })
      })
  }
}