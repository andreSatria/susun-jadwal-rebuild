/*
 *
 * LogoutModule
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { logOut } from 'containers/LoginPage/actions';

class LogoutModule extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    logOut: PropTypes.func,
    push: PropTypes.func,
  };

  componentDidMount() {
    document.cookie = "major_id=; expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/";
    document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/";
    document.cookie = "user_id=; expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/";
    this.props.logOut();
    this.props.push('/');
  }

  render() {
    return (
      <div>
          <p>Logout...</p>
      </div>
    );
  }
}


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    logOut: () => dispatch(logOut()),
    push: (url) => dispatch(push(url)),
  };
}

export default connect(null, mapDispatchToProps)(LogoutModule);
