/* eslint-disable */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { AppContainer } from './style';

// import NavBar from 'components/NavBar';
// import SideMenu from 'components/SideMenu';
// import DarkenBackground from 'components/DarkenBackground';
import NavBar from 'containers/NavigationMenu'


import { theme } from './theme';
import { routes } from './routes';

export default class App extends React.Component {
  render() {
    const pages = routes.map(route => (
      <Route
        component={route.component}
        exact={route.exact}
        path={route.path}
      />
    ));

    return (
      <ThemeProvider theme={theme}>
        <AppContainer>
        { window.location.pathname === '/' ? null :
          <NavBar />
        }
            <Switch>
                {pages}
            </Switch>
        </AppContainer>
      </ThemeProvider>
    );
  }
}
