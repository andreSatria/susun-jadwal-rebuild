import NotFoundPage from 'containers/NotFoundPage';
import PickSchedulePage from 'containers/PickSchedulePage';
import LoginPage from 'containers/LoginPage';
import ShowSchedulePage from 'containers/ShowSchedulePage';
import LogoutModule from 'containers/LogoutModule';


export const routes = [
  {
    'component': LogoutModule,
    'exact': true,
    'path': '/logout'
  },
  {
    'component': PickSchedulePage,
    'exact': true,
    'path': '/susun'
  },
  {
    'component': ShowSchedulePage,
    'exact': true,
    'path': '/jadwal/:slug'
  },
  {
    'component': ShowSchedulePage,
    'exact': true,
    'path': '/jadwal'
  },
  {
    'component': LoginPage,
    'exact': true,
    'path': '/',
  },
  { 'component': NotFoundPage }
];
