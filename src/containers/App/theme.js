export const theme = {
  'border': {
    'borderRadius': '18px',
    'shadowColor': 'rgba(36,37,38,0.13)',
    'shadowConfig': '0 0.25rem 1rem'
  },
  'colors': {
    'black': '#000000',
    'blue': '#3C8CCE',
    'darkBlue': '#2C4E73',
    'lightGrey': '#CBCBCB',
    'lightYellow': '#FAED85',
    'pink': '#FBC7A5',
    'white': '#FFFFFF',
    'yellow': '#FFCC33'
  }
};
