import styled from 'styled-components';

export const AppContainer = styled.div`
  position: relative;
  width: 100%;
  max-width: 100%;
  overflow: hidden;
  min-height: 100vh;
  z-index: 1;

`;
