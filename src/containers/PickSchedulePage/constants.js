export const DEFAULT_ACTION = 'src/PickSchedulePage/DEFAULT_ACTION';
export const FETCH_COURSES = 'src/PickSchedulePage/FETCH_COURSES';
export const FETCH_COURSES_SUCCESS = 'src/Homepage/FETCH_COURSES_SUCCESS';
export const FETCH_COURSES_FAILED = 'src/Homepage/FETCH_COURSES_FAILED';
export const ADD_CLASS = 'src/PickSchedulePage/ADD_CLASS';
export const REMOVE_CLASS = 'src/PickSchedulePage/REMOVE_CLASS';
