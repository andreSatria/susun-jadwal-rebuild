import React from 'react';
import { push } from 'react-router-redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { PickSchedulePageContainer } from './style';
import CourseItem from 'components/CourseItem';
import Loading from 'components/Loading';
import { fetchCourses, addClassSchedule, removeClassSchedule} from 'containers/PickSchedulePage/actions';
import { search, menuDecider } from "containers/NavigationMenu/actions";
import {
  getCourses,
  isLoadingCourses,
  isLoadedCourses,
  getAddedClasses,
  getQuery,
  getMenu,
  getUser,
} from 'selectors';

import { getCookie } from 'utils';
import { setLoginData } from '../LoginPage/actions';

class PickSchedulePage extends React.Component {

    UNSAFE_componentWillMount() {
        this.props.indicateMenu(true);
    }

  componentDidMount() {
    const major_id = getCookie('major_id');
    const token = getCookie('token');
    const user_id = getCookie('user_id');
    if (!(major_id !== '') || !(token !== '') || !(user_id !== '')) {
      this.props.push('/');
    } else {
      if (this.props.user.major_id === '' && this.props.user.token === '' && this.props.user.user_id === '') {
        this.props.setLoginData(major_id, token, user_id);
      }
    }

    if (!this.props.isLoadedCourses) {
      this.props.fetchCourses();
      this.props.search();
    }
  }

  pickClass = (classSchedule) => {
    this.props.removeClassSchedule(classSchedule);
    this.props.addClassSchedule(classSchedule);
  }

  searchFunction = (matkul) => {
      if (!matkul) {
         return this.props.course;
        }
      let query = !this.props.query ? "" : this.props.query;
      query = query.replace(/\\/g, "\\\\");
      const filteredCourse = matkul.slice().filter((matkulName) => {
          return matkulName.name.toLowerCase().match(query.toLowerCase());
      })

      return filteredCourse;
  }

  render() {
    let { courses, addedClasses } = this.props;

    const filtered = this.searchFunction(courses);

      if (this.props.isLoadingCourses) {
        return (
            <PickSchedulePageContainer>
                <div className="midLoader">
                    <Loading/>
                </div>
            </PickSchedulePageContainer>
        );
      }


    return (
      <PickSchedulePageContainer>
        <div className="scheduleContainer">
          { filtered.length > 0 ?
            filtered.map((item, index) =>
              <CourseItem
                key={index}
                name={item.name}
                sks={item.sks}
                term={item.term}
                classes={item.class}
                pickClass={this.pickClass}
                addedClass={addedClasses.find((cl) => cl.course === item.name)}
              />
            )
            :
            <div className="scheduleContainer">
                <div className="centerize">
                    <h1>The Course <i>"{this.props.query}"</i> is unavailable</h1>
                </div>
            </div>
          }
        </div>
      </PickSchedulePageContainer>
    );
  }
}

PickSchedulePage.propTypes = {
  addClassSchedule: PropTypes.shape().isRequired,
  addedClasses: PropTypes.shape().isRequired,
  courses: PropTypes.shape().isRequired,
  fetchCourses: PropTypes.func.isRequired,
  indicateMenu: PropTypes.func.isRequired,
  isLoadedCourses: PropTypes.bool.isRequired,
  isLoadingCourses: PropTypes.bool.isRequired,
  removeClassSchedule: PropTypes.shape().isRequired,
  sideDecider: PropTypes.bool.isRequired,
  user: PropTypes.shape().isRequired,
};


function mapStateToProps(state) {
  return {
    addedClasses: getAddedClasses(state),
    courses: getCourses(state),
    isLoadedCourses: isLoadedCourses(state),
    isLoadingCourses: isLoadingCourses(state),
    query: getQuery(state),
    sideDecider: getMenu(state),
    user: getUser(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addClassSchedule: (classSchedule) => dispatch(addClassSchedule(classSchedule)),
    fetchCourses: () => dispatch(fetchCourses()),
    indicateMenu: (bool) => dispatch(menuDecider(bool)),
    push: (url) => dispatch(push(url)),
    removeClassSchedule: (classSchedule) => dispatch(removeClassSchedule(classSchedule)),
    search: (query) => dispatch(search(query)),
    setLoginData: (majorId, token, userId) => dispatch(setLoginData(majorId, token, userId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PickSchedulePage);
