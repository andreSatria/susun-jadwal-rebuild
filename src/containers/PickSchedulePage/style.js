import styled from 'styled-components';
import BACKGROUND from 'assets/backgroundDesktop.jpg';
import BACKGROUNDMOBILE from 'assets/backgroundMobile.jpg';

export const PickSchedulePageContainer = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-image: url(${BACKGROUND});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

  .scheduleContainer {
    width: 100%;
    padding: 5rem;

    .centerize{
        font-size:0.75rem;
        display:flex;
        justify-content:center;
        align-items:center;
    }

    i{
        color:${props => props.theme.colors.darkBlue};
    }

  }



  .midLoader{
      width:100%;
      min-height:100vh;
      display:flex;
      justify-content:center;
      align-items:center;

  }

  @media (max-width:768px) {
    background-image: url(${BACKGROUNDMOBILE});

    .scheduleContainer {
      width: 100%;
      padding:5rem 2rem 2rem 2rem;

      .centerize{
          font-size:0.45rem;
      }
    }
  }
`;
