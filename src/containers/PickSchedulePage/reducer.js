import { fromJS } from 'immutable';
import {
  FETCH_COURSES,
  FETCH_COURSES_SUCCESS,
  FETCH_COURSES_FAILED,
  ADD_CLASS,
  REMOVE_CLASS
} from './constants';

const initialState = fromJS({
  addedClasses: [],
  courses: [],
  error: null,
  isLoaded: false,
  isLoading: false,
});

function pickSchedulePageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_COURSES:
      return state.set('isLoading', true)
    case FETCH_COURSES_SUCCESS:
      return state.set('courses', action.payload.courses).set('isLoading', false)
        .set('isLoaded', true);
    case FETCH_COURSES_FAILED:
      return state.set('error', action.payload).set('isLoading', false)
        .set('isLoaded', false);
    case ADD_CLASS:
      return state.set('addedClasses', [...state.get('addedClasses'), action.payload])
    case REMOVE_CLASS:
      return state.set('addedClasses', state.get('addedClasses').filter((classSchedule) => classSchedule.course !== action.payload.course))
    default:
      return state;
  }
}

export default pickSchedulePageReducer;
