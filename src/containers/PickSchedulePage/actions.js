import axios from 'axios';
import swal from 'sweetalert';

import {
  FETCH_COURSES,
  FETCH_COURSES_SUCCESS,
  FETCH_COURSES_FAILED,
  ADD_CLASS,
  REMOVE_CLASS
} from './constants'

import { fetchCoursesApi } from 'api';
import { getAuthUser } from 'selectors';

export function fetchCourses() {
  const user = getAuthUser();

  axios.defaults.headers.common.Authorization = `JWT ${user.token}`;

  return (dispatch) => {
    dispatch({ type: FETCH_COURSES });

    const majorId = user.major_id;

    axios.get(fetchCoursesApi(majorId))
    .then((response) => {
      dispatch({
        payload: response.data,
        type: FETCH_COURSES_SUCCESS,
      })
    })
    .catch((error) => {
      dispatch({
        payload: error,
        type: FETCH_COURSES_FAILED,
      })
    })
  }
}

export function addAgendaSchedule(agenda) {
  return (dispatch) => {
    dispatch({
      payload: agenda,
      type: ADD_CLASS,
    });
    swal("Submit Success!", "Agenda kamu telah ditambah!", "success");
  }
}

export function addClassSchedule(classSchedule) {
  return (dispatch) => {
    dispatch({
      payload: classSchedule,
      type: ADD_CLASS,
    });
  }
}

export function removeClassSchedule(classSchedule) {
  return (dispatch) => {
    dispatch({
      payload: classSchedule,
      type: REMOVE_CLASS,
    });
  }
}