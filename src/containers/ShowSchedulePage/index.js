/* eslint-disable sort-keys, radix, react/jsx-key, object-property-newline, no-lonely-if */

import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import equal from 'fast-deep-equal'
import { connect } from 'react-redux';
import { ShowSchedulePageContainer } from './style';
import PickedCourse from 'components/PickedCourse';
import Loading from 'components/Loading';
import { fetchPickedCourses } from './actions';
import { menuDecider } from "containers/NavigationMenu/actions";
import {
  getPickedCourses,
  isLoadingPickedCourses,
  isLoadedPickedCourses,
  getMenu,
  getUser
} from 'selectors';

import { getCookie } from 'utils';
import { setLoginData } from '../LoginPage/actions';

class ShowSchedulePage extends React.Component {

  UNSAFE_componentWillMount() {
    this.props.indicateMenu(false);
  }

  componentDidMount() {
    const major_id = getCookie('major_id');
    const token = getCookie('token');
    const user_id = getCookie('user_id');
    if (!(major_id !== '') || !(token !== '') || !(user_id !== '')) {
      this.props.push('/');
    } else {
      if (this.props.user.major_id === '' && this.props.user.token === '' && this.props.user.user_id === '') {
        this.props.setLoginData(major_id, token, user_id);
      }
    }

    this.props.fetchPickedCourses(this.getParam());
  }

componentDidUpdate(prevProps) {
  if (!equal(this.props.pickedCourses, prevProps.pickedCourses)) {
    this.props.fetchPickedCourses(this.getParam());
  }
}

  convertToMinute(val) {
    const temp = val.split(".");
    const hour = parseInt(temp[0]);
    const minute = parseInt(temp[1]);

    return (hour * 60) + minute;
  }

  getParam = () => {
    const params = this.props.location.pathname.split('/')

    return params;
  }

  render() {
    let { pickedCourses } = this.props
    if (this.props.isLoadingPickedCourses) {
      return (
          <ShowSchedulePageContainer>
              <div className="midLoader">
                  <Loading/>
              </div>
          </ShowSchedulePageContainer>
      );
    }

    if (!pickedCourses) {
      pickedCourses = []
    }


    const primarySchedElem = pickedCourses.map((course) => {
      const dur = this.convertToMinute(course.end) - this.convertToMinute(course.start);
      const start = this.convertToMinute(course.start) - this.convertToMinute('08.00');

      return (
        <PickedCourse
          name={course.name}
          day={course.day}
          start={course.start}
          end={course.end}
          room={course.room}
          dur={dur}
          initialStart={start}
        />
      );
    });

    return (
      <ShowSchedulePageContainer>
      {/* <div style={{height: '5rem', width: '100%', background: 'rgb(255,255,255,0)'}}>
      </div> */}
      <div className="bodyContainer">
        <div className="jadwal">
          <div className="scheduleModule">
            <div className="scheduleHeader">
              <div className="dayHeader">
                <p>Senin</p>
              </div>
              <div className="dayHeader">
                <p>Selasa</p>
              </div>
              <div className="dayHeader">
                <p>Rabu</p>
              </div>
              <div className="dayHeader">
                <p>Kamis</p>
              </div>
              <div className="dayHeader">
                <p>Jumat</p>
              </div>
              <div className="dayHeader">
                <p>Sabtu</p>
              </div>
            </div>
            <div className="scheduleContent">
              <div className="timeContent">
                <div className="container">
                  <p>08.00</p>
                  <p>08.30</p>
                  <p>09.00</p>
                  <p>09.30</p>
                  <p>10.00</p>
                  <p>10.30</p>
                  <p>11.00</p>
                  <p>11.30</p>
                  <p>12.00</p>
                  <p>12.30</p>
                  <p>13.00</p>
                  <p>13.30</p>
                  <p>14.00</p>
                  <p>14.30</p>
                  <p>15.00</p>
                  <p>15.30</p>
                  <p>16.00</p>
                  <p>16.30</p>
                  <p>17.00</p>
                  <p>17.30</p>
                  <p>18.00</p>
                </div>
              </div>
                <div>
                  <div className="garis"></div>
                  <div className="garis" style={{top: '187px'}}></div>
                  <div className="garis" style={{top: '219px'}}></div>
                  <div className="garis" style={{top: '251px'}}></div>
                  <div className="garis" style={{top: '283px'}}></div>
                  <div className="garis" style={{top: '315px'}}></div>
                  <div className="garis" style={{top: '347px'}}></div>
                  <div className="garis" style={{top: '379px'}}></div>
                  <div className="garis" style={{top: '411px'}}></div>
                  <div className="garis" style={{top: '443px'}}></div>
                  <div className="garis" style={{top: '475px'}}></div>
                  <div className="garis" style={{top: '507px'}}></div>
                  <div className="garis" style={{top: '539px'}}></div>
                  <div className="garis" style={{top: '571px'}}></div>
                  <div className="garis" style={{top: '603px'}}></div>
                  <div className="garis" style={{top: '635px'}}></div>
                  <div className="garis" style={{top: '667px'}}></div>
                  <div className="garis" style={{top: '699px'}}></div>
                  <div className="garis" style={{top: '731px'}}></div>
                  <div className="garis" style={{top: '763px'}}></div>
                  <div className="garis" style={{top: '795px'}}></div>
                </div>
                <div className="dayContent">
                  {primarySchedElem}
                </div>
              </div>
            </div>
          </div>
        </div>
      </ShowSchedulePageContainer>
    );
  }
}

ShowSchedulePage.propTypes = {
  pickedCourses: PropTypes.shape().isRequired,
  fetchPickedCourses: PropTypes.func.isRequired,
  isLoadedPickedCourses: PropTypes.bool.isRequired,
  isLoadingPickedCourses: PropTypes.bool.isRequired,
  indicateMenu: PropTypes.func.isRequired,
  sideDecider: PropTypes.bool.isRequired,
  params: PropTypes.shape(),
  setLoginData: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  location: PropTypes.shape().isRequired,
}

function mapStateToProps(state) {
  return {
    pickedCourses: getPickedCourses(state),
    isLoadedPickedCourses: isLoadedPickedCourses(state),
    isLoadingPickedCourses: isLoadingPickedCourses(state),
    sideDecider: getMenu(state),
    user: getUser(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchPickedCourses: (params) => dispatch(fetchPickedCourses(params)),
    indicateMenu: (bool) => dispatch(menuDecider(bool)),
    push: (url) => dispatch(push(url)),
    setLoginData: (majorId, token, userId) => dispatch(setLoginData(majorId, token, userId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowSchedulePage);
