/* eslint-disable react/jsx-key, object-property-newline, template-curly-spacing */

import styled from 'styled-components';
import BACKGROUND from 'assets/backgroundDesktop.jpg';
import BACKGROUNDMOBILE from 'assets/backgroundMobile.jpg';

export const ShowSchedulePageContainer = styled.div`

.bodyContainer {
  background-image:url(${BACKGROUND});
  width: 100%;
  min-height:100vh;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

.midLoader{
    width:100%;
    min-height:100vh;
    display:flex;
    justify-content:center;
    align-items:center;
    background-image:url(${BACKGROUND});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}


.jadwal { /* stylelint-disable */
  width: 100%;
  height: auto;
  overflow-x: auto;
  padding: 7rem;
  padding-top: 6rem;
}

@media (max-width:1366px) {
  .jadwal {
    padding: 4.8rem;
    padding-top: 6rem;
  }

  .garis {
    margin-left: 0.5rem;
  }
}

@media (max-width:1281px) {
  .jadwal {
    padding: 4.4rem;
    padding-top: 6rem;
  }
}

@media (max-width:1025px) {
  .jadwal {
    padding: 3.2rem;
    padding-top: 2rem;
  }

  .garis {
    margin-left: 1.2rem;
  }
}

@media (max-width:945px) {
  .jadwal {
    padding: 2.6rem;
    padding-top: 2rem;
  }
  .garis {
    margin-left: 1.5rem;
  }

  .timeContent .container p {
    font-size: 14px;
  }
}

@media (max-width:768px) {
  .bodyContainer {
    background-image:url(${BACKGROUNDMOBILE});
  }
  .scheduleHeader p {
    font-size: 14px;
  }
  .jadwal {
    padding: 2rem;
    padding-top: 2rem;
  }

  .timeContent .container p {
    font-size: 13px;
  }
}

@media (max-width:600px) {
  .jadwal {
    padding: 1rem;
    padding-top: 2.5rem;    
  }

  .timeContent .container p {
    font-size: 11.5px;
  }
}

@media (max-width:480px) {
  .jadwal {
    padding-top: 2.5rem;
  }
  .scheduleHeader .dayHeader p {
    font-size: 11px;
    padding-left: 2rem;
  }

  .timeContent .container p {
    font-size: 10px;
  }
}

@media (max-width:414px) {
  .jadwal {
    padding: 0rem;
    padding-top: 2.5rem;    
  }

  .scheduleHeader .dayHeader p {
    font-size: 11px;
    padding-left: 2rem;
  }

  .timeContent .container p {
    font-size: 10px;
  }
}

@media (max-width:340px) {
  .scheduleHeader .dayHeader p {
    font-size: 10px;
    padding-left: 1.5rem;
  }
}

.scheduleModule {
  width: 100%%;
  ${'' /* min-width: 1366px; */}
  height: auto;
}

.scheduleHeader {
  width: 100%;
  height: auto;
  background: #245472;
  color: #FFFFFF;
  font-size: 18px;
  text-align: center;
}

.scheduleHeader p {
  margin: 0;
  line-height: 1;
  padding-left: 3rem;
}

.timeHeader,
.dayHeader,
.timeContent,
.dayContent {
  display: inline-block;
  margin: 0;
}

.dayHeader {
  width: 16%;
  padding: 0.75rem 0;
}

.scheduleContent {
  margin-top: 1rem;
  display: flex;
  flex-direction: row;
}

.timeContent {
  width: 3.5%;
  text-align: left;
  padding: 0;
  height: 42rem;
  background: rgb(255,255,255,0);
  position: relative;
}

.timeContent .container {
  position: absolute;
  padding-left: 0.25rem;
  width: 100%;
  top: 0;
  right: 0;
  left: 0;
}

.garis {
  border-bottom-style: solid;
  width: 85%;
  border-width: 1px;
  height: 1rem;
  border-color: #cccc;
  padding: 10px;
  position: absolute;
  padding-top: 21.5px;
}
.timeContent p {
  margin: 0;
  padding: 0.5rem 0;
  font-size: 1rem;
  color: #333;
  font-weight: bold;
  background: rgb(255,255,255,0);
  line-height: 1;
}

.dayContent {
  width: 90%;
  background: rgb(255,255,255,0);
  height: 42rem;
  position: relative;
}


@media (max-width:1024px){
    .bodyContainer{
        padding-top:4rem;
    }
}
`;
