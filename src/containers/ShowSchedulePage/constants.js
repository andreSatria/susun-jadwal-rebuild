export const DEFAULT_ACTION = 'src/ShowSchedulePage/DEFAULT_ACTION';
export const FETCH_PICKED_COURSES = 'src/ShowSchedulePage/FETCH_PICKED_COURSES';
export const FETCH_PICKED_COURSES_SUCCESS = 'src/ShowSchedulePage/FETCH_PICKED_COURSES_SUCCESS';
export const FETCH_PICKED_COURSES_FAILED = 'src/ShowSchedulePage/FETCH_PICKED_COURSES_FAILED';
export const CURRENT_ACTIVE_JADWAL = 'src/ShowSchedulePage/CURENT_ACTIVE_JADWAL';
