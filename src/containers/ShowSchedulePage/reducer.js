import { fromJS } from 'immutable';
import {
  FETCH_PICKED_COURSES,
  FETCH_PICKED_COURSES_SUCCESS,
  FETCH_PICKED_COURSES_FAILED,
  CURRENT_ACTIVE_JADWAL,
} from './constants';

const initialState = fromJS({
  activeJadwal: null,
  error: null,
  isLoaded: false,
  isLoading: false,
  pickedCourses: [],
});

function pickSchedulePageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PICKED_COURSES:
      return state.set('isLoading', true)
    case FETCH_PICKED_COURSES_SUCCESS:
      return state.set('pickedCourses', action.payload.jadwals).set('isLoading', false)
        .set('isLoaded', true);
    case FETCH_PICKED_COURSES_FAILED:
      return state.set('error', action.payload).set('isLoading', false)
        .set('isLoaded', false);
    case CURRENT_ACTIVE_JADWAL:
    return state.set('activeJadwal', action.payload)

    default:
      return state;
  }
}

export default pickSchedulePageReducer;
