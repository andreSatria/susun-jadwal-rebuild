import axios from 'axios';
import { push } from 'react-router-redux';

import {
  FETCH_PICKED_COURSES,
  FETCH_PICKED_COURSES_SUCCESS,
  FETCH_PICKED_COURSES_FAILED,
  CURRENT_ACTIVE_JADWAL,
} from './constants'

import { getAuthUser } from 'selectors';
import { fetchJadwalApi, fetchJadwalUserApi } from 'api';

export function fetchPickedCourses(params, isJadwal = true) {

  const user = getAuthUser();
  const jadwalId = params[2];

  return (dispatch) => {

    dispatch({ type: FETCH_PICKED_COURSES });
    axios.get(fetchJadwalUserApi(user.user_id))
    .then((response) => {

      const jadwals = response.data.jadwals;
      const jadwalUtama = jadwals.find(jadwal => jadwal.utama)

      dispatch({
        payload: jadwalUtama,
        type: CURRENT_ACTIVE_JADWAL,
      })
      let jadwalShown = jadwalUtama.id;

      if (jadwalId) {
        jadwalShown = jadwalId;
      } else if (isJadwal) {
          dispatch(push(`/jadwal/${jadwalShown}`))
      }

      axios.get(fetchJadwalApi(jadwalShown))
        .then((resp) => {
          dispatch({
            payload: resp.data,
            type: FETCH_PICKED_COURSES_SUCCESS,
          })
        })
        .catch((error) => {
          dispatch({
            payload: error,
            type: FETCH_PICKED_COURSES_FAILED,
          })
        })
    })
    .catch((error) => {
      dispatch({
        payload: error,
        type: FETCH_PICKED_COURSES_FAILED,
      })
    })
  }
}
