import styled from 'styled-components';

export const NavBarStyle = styled.div`

.navbar{
  overflow:hidden;
  height:auto;
  ${props => props.sideDecider && `
    position:fixed;
  `}
  ${props => !props.sideDecider && `
    position:absolute;
  `}
  width:100%;
  min-height:3rem;
  justify-content:space-between;
  align-items:space-evenly;
  display:flex;
  flex-direction:column;
  background:rgba(255, 255, 255, 0.65);

  .wrapper{
      padding:1rem 0;
      margin-bottom:-1rem;
      height:3.75rem;
      width:100%;
      justify-content:space-between;
      align-items:space-evenly;
      display:flex;
  }

  .searchMobile{
      width:100%;
      padding: 0.75rem 0 0 0;
      justify-content:flex-end;
      align-items:flex-end;
      display:flex;


      .searchBar{
        height:1.5rem;
        border-top-left-radius: 5em;
        border-bottom-left-radius: 5em;
        font-size:0.75rem;
        padding: 0.5em 0em 0.5em 1.25em;
        border: solid 2px ${props => props.theme.colors.darkBlue};
        border-right:none;
        background:white;
        box-sizing: border-box;
        background: white;
        color:${props => props.theme.colors.black};
      }

      input::placeholder{
          color: ${props => props.theme.colors.darkBlue}
      }
  }


  .menuButton{
    flex:1;
    background:transparent;
    border:none;
    display:flex;
    align-items:flex-start;

    img{
      width:1.75rem;
      margin-left:1rem;
      height:auto;
    }
  }

  .logoContainer{
    flex:2;
    display:flex;
    justify-content: center;
    align-items: center;

    img{
      width:15rem;
    }
  }

  .searchContainer{
    display:flex;
    flex:1;
    justify-content: flex-end;
    align-items: center;


    .searchBar{
      width:60%;
      height:2rem;
      border-radius:5em;
      font-size:0.75rem;
      padding: 0.5em 0em 0.5em 1.25em;
      border: solid 2px #cecfcf;
      background:white;
      box-sizing: border-box;
      background: white;
    }

    .searchButton{
      width:3rem;
      height:2rem;
      background:transparent;
      border:none;

      img{
        width:2.25rem;
        height:auto;
        background-size:contain;

      }
    }

  }

  @media (max-width: 728px){

    .menuButton{
      img{
        width:1.5rem;
      }
    }
    .logoContainer{
      img{
        width:10rem;
      }
    }

    .searchContainer{
      .searchBar{
        display:none;
      }

      .searchButton {
        img{
          width:1.75rem;
        }
      }

    }

  }

}




`;
