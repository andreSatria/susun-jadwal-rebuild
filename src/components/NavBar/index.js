import React from 'react';
import { NavBarStyle } from './style';
import LOGO from './../../assets/logos/logoheader2.png';
import SEARCH from './../../assets/buttons/searchicon(header).png';
import MENUIMG from './../../assets/buttons/menuicon.png';

class NavBar extends React.Component {

    state={
        opened:false,
    }

    searchToggler = () => {
        return this.setState((prev) => {
            return {opened : !prev.opened };
        });
    }


  render() {
    return (
      <NavBarStyle sideDecider={this.props.sideDecider}>
        <div className="navbar">
            <div className="wrapper">
                <button className="menuButton" onClick={this.props.menuToggler}>
                  <img src={MENUIMG} alt="menu" />
                </button>
                <div className="logoContainer">
                  <img src={LOGO} alt="logo" />
                </div>
                    <div className="searchContainer">
                    { this.props.sideDecider ?
                        <React.Fragment>
                        { this.props.mobile
                            ? null
                            : <input className="searchBar" type="text" placeholder="Cari Mata Kuliah" onChange={(e) => this.props.search(e.target.value)}></input>
                        }
                          <button className="searchButton">
                            <img src={SEARCH} alt="search" onClick={this.searchToggler} />
                          </button>
                          </React.Fragment>
                         :
                        null
                   }
                    </div>
            </div>
            { this.props.sideDecider ?
                <React.Fragment>
                <div className="searchMobile">
                    { this.props.mobile
                        ? <React.Fragment>
                            { this.state.opened
                                ? <input className="searchBar" type="text" placeholder="Cari Mata Kuliah" onChange={(e) => this.props.search(e.target.value)}></input>
                                : null
                            }
                        </React.Fragment>
                        : null
                    }
                    </div>
                </React.Fragment>
                :
                null
            }
        </div>
      </NavBarStyle>

    );
  }
}

export default NavBar;
