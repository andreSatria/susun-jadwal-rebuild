/* eslint-disable sort-keys, radix, react/jsx-key, object-property-newline */

import React from 'react';
import PropTypes from 'prop-types';
import { PickedCourseContainer } from './style';

class PickedCourse extends React.Component {

  render() {
    const { name, day, start, end, room, dur, initialStart } = this.props;

    return (
      <PickedCourseContainer>
        <div key={`entryJadwal-${name}-${day.toLowerCase()}-${start}-${end}-${room}`}
          className={`entryJadwal ${day.toLowerCase()}`}
          style={{height: `${(dur / 30) * 2}rem`, top: `${(initialStart / 30) * 2}rem`}}>
          <div className="containerInner">
            <p className="courseProperty">{start} - {end}</p>
            <p className="courseName">{name}</p>
            <p className="courseProperty">{room}</p>
          </div>
        </div>
      </PickedCourseContainer>
    );
  }
}

PickedCourse.propTypes = {
  'name': PropTypes.string.isRequired,
  'day': PropTypes.string.isRequired,
  'start': PropTypes.string.isRequired,
  'end': PropTypes.string.isRequired,
  'room': PropTypes.string.isRequired,
  'dur': PropTypes.number.isRequired,
  'initialStart': PropTypes.string.isRequired,
}

export default PickedCourse;
