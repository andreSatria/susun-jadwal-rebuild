import styled from 'styled-components';

export const PickedCourseContainer = styled.div`

.entryJadwal {
  position: absolute;
  width: calc((100% / 6) - 0.5rem);
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: #FFCC33;
  box-shadow: 0 0.8rem 1.5rem rgba(36,37,38, 0.5);
  border-radius: 18px;
  padding: 10px;
  font-size: 14px;
}

@media (max-width:1366px) {
  .entryJadwal {
    font-size: 13px;
  }
}

@media (max-width:1024px) {
  .entryJadwal {
    font-size: 12px;
  }
}

@media (max-width:768px) {
  .entryJadwal {
    font-size: 10px;
  }
}

@media (max-width:600px) {
  .entryJadwal {
    text-align: center;
    font-size: 9px;
    padding: 5px;
  }
}

@media (max-width:414px) {
  .entryJadwal {
    padding: 0px;
    font-size: 8px;
  }
}

.senin {
  left: 1.25rem;
}

.selasa {
  left: calc(((100% / 6) * 1.05) + 1.25rem);
}

.rabu {
  left: calc(((100% / 6) * 2.12) + 1.25rem);
}

.kamis {
  left: calc(((100% / 6) * 3.18) + 1.25rem);
}

.jumat {
  left: calc(((100% / 6) * 4.25) + 1.25rem);
}

.sabtu {
  left: calc(((100% / 6) * 5.3) + 1.25rem);
}

.courseProperty {
    margin: 0;
    margin-bottom: 2.5px;
  }

.courseName {
  font-weight: bold;
  margin: 0;
  margin-bottom: 2.5px;
}
`;