/* eslint-disable complexity, react/jsx-key, object-property-newline, template-curly-spacing, sort-keys, prefer-const, dot-notation */

import React, { Component } from 'react';
import CloseIcon from '../../assets/closeIcon.png';
import Back from '../../assets/buttons/back.png';
import styles from './styles.css';

class AgendaModal extends Component {
  constructor(props) {
    super();
    this.state = {
      name: '',
      schedule: {
        day: 'Senin',
        start: '',
        end: '',
        room: '',
      },
      sks: 0,
        errMesage: {
          name: '',
          schedule: {
            day: '',
            start: '',
            end: '',
            room: '',
          },
        },
    };
    // this.handleClickDate = this.handleClickDate.bind(this)
  }

  componentWillMount() {
    document.addEventListener('mousedown', this.handleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClick, false);
  }

  handleClick = (evt) => {
    if (!this.node.contains(evt.target)) {
      this.props.onClickAddAgenda(false);
    }
  }

  agendaChangeInput(field, value) {
    const currentState = this.state;
    let newState = currentState;

    if (Array.isArray(field)) {
      newState[field[0]][field[1]] = value;
    } else {
      newState[field] = value;
    }

    this.setState({
      ...newState,
    });
  }

  onClickCloseAgenda = () => {
    this.props.onClickAddAgenda(false);
  }

  // mobileButton = () => {
  //
  //     return days.map((day,i) =>{
  //         <div className={this.state.schedule.day === day ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], day)}>days</div>
  //     })
  // }

  addAgenda = () => {
    const timeChecker = /^([0-1]?[0-9]|2[0-4])\.([0-5][0-9])(\.[0-5][0-9])?$/;

    const nameCheck = this.state.name === '' || this.state.name === null;
    const dayCheck = this.state.schedule.day === '' || this.state.schedule.day === null;
    const startTimeCheck = timeChecker.test(this.state.schedule.start);
    const endTimeCheck = timeChecker.test(this.state.schedule.end);
    const roomCheck = this.state.schedule.room === '' || this.state.schedule.room === null;

    if (startTimeCheck && endTimeCheck && !nameCheck && !dayCheck && !roomCheck) {

      const agenda = {
        name: this.state.name,
        jadwal: [
          {
            day: this.state.schedule.day,
            start: this.state.schedule.start,
            end: this.state.schedule.end,
            room: this.state.schedule.room
          }
        ]
      }

      this.props.addAgenda({
        class: agenda,
        course: `${this.state.name} - ${this.state.schedule.day} - ${this.state.schedule.start} - ${this.state.schedule.end} - ${this.state.schedule.room}`,
        sks: this.state.sks
      }, true);

      this.finishAddAgenda();
    } else {
      const currentState = this.state;
      let newState = currentState;

      if (nameCheck) {
        newState['errMesage']['name'] = 'Nama agenda tidak boleh kosong';
      } else {
        newState['errMesage']['name'] = '';
      }

      if (dayCheck) {
        newState['errMesage']['schedule']['day'] = 'Hari tidak boleh kosong';
      } else {
        newState['errMesage']['schedule']['day'] = '';
      }

      if (!startTimeCheck) {
        newState['errMesage']['schedule']['start'] = 'Format jam salah, seharusnya HH.MM, contoh: 12.30';
      } else {
        newState['errMesage']['schedule']['start'] = '';
      }

      if (!endTimeCheck) {
        newState['errMesage']['schedule']['end'] = 'Format jam salah, seharusnya HH.MM, contoh: 12.30';
      } else {
        newState['errMesage']['schedule']['end'] = '';
      }

      if (roomCheck) {
        newState['errMesage']['schedule']['room'] = 'Lokasi tidak boleh kosong';
      } else {
        newState['errMesage']['schedule']['room'] = '';
      }

      this.setState({
        newState,
      });
    }
  }

  finishAddAgenda = () => {
    this.setState({
      name: '',
      schedule: {
        day: 'Senin',
        start: '',
        end: '',
        room: '',
      },
      sks: 0,
      errMesage: {
        name: '',
        schedule: {
          day: '',
          start: '',
          end: '',
          room: '',
        },
      },
    });
    this.onClickCloseAgenda(false);
  }

  handleKeyPress = (target) => {
    if (target.charCode === 13) {
      this.addAgenda();
    }

}

  render() {
    const { name, schedule } = this.state;
    const { day, start, end, room } = schedule;
    const days = ["Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"]


    return (
      <div className="modal-overlay">
        <div className="modal" ref={node => this.node = node}>
        {
            !this.props.mobile ?
            <React.Fragment>
                <div className="close-button" onClick={this.onClickCloseAgenda}>
                    <img src={CloseIcon} />
                </div>
            </React.Fragment>

            :
            <React.Fragment>
                <div className="close-mobile" onClick={this.onClickCloseAgenda}>
                    <img src={Back} />
                </div>
            </React.Fragment>
        }
          <div className="container" onKeyPress={this.handleKeyPress}>
            <div className="title">
              Tambah Agenda
            </div>
            <div className="form">
              <div className="left-form">
                <div className="input-form">
                  <div className="agenda-title">Nama Agenda</div>
                  <h3 style={{ display: this.state.errMesage.name === '' ? 'none' : 'block'}}>{this.state.errMesage.name}</h3>
                  <input value={name} onChange={(evt) => this.agendaChangeInput('name', evt.target.value)} placeholder="Rapat Compfest X" />
                </div>
                <div>
                  <div className="input-form">
                    <div className="agenda-title">Jam Mulai</div>
                    <h3 style={{ display: this.state.errMesage.schedule.start === '' ? 'none' : 'block'}}>{this.state.errMesage.schedule.start}</h3>
                    <input value={start} onChange={(evt) => this.agendaChangeInput(['schedule', 'start'], evt.target.value)} placeholder="format HH.MM" />
                  </div>
                </div>
              </div>
              <div className="right-form">
                <div>
                  <div className="input-form">
                    <div className="agenda-title">Lokasi</div>
                    <h3 style={{ display: this.state.errMesage.schedule.room === '' ? 'none' : 'block'}}>{this.state.errMesage.schedule.room}</h3>
                    <input value={room} onChange={(evt) => this.agendaChangeInput(['schedule', 'room'], evt.target.value)} placeholder="Belyos" />
                  </div>
                </div>
                <div>
                  <div className="input-form">
                    <div className="agenda-title">Jam Selesai</div>
                    <h3 style={{ display: this.state.errMesage.schedule.end === '' ? 'none' : 'block'}}>{this.state.errMesage.schedule.end}</h3>
                    <input value={end} onChange={(evt) => this.agendaChangeInput(['schedule', 'end'], evt.target.value)} placeholder="format HH.MM" />
                  </div>
                </div>
              </div>
            </div>
            <div className="date">
              <div>
                Hari
              </div>
              { !this.props.mobile ?
                  <div className="button-array">
                <React.Fragment>
                    <button className={day === 'Senin' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Senin')}>Senin</button>
                    <button className={day === 'Selasa' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Selasa')}>Selasa</button>
                    <button className={day === 'Rabu' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Rabu')}>Rabu</button>
                    <button className={day === 'Kamis' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Kamis')}>Kamis</button>
                    <button className={day === 'Jumat' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Jumat')}>Jumat</button>
                    <button className={day === 'Sabtu' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Sabtu')}>Sabtu</button>
                </React.Fragment>
                </div>
                :
                <React.Fragment>
                <div className="mobileCont">
                    {
                        days.map(dayName => <div className={day === dayName ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], dayName)}><p>{dayName}</p></div>)
                    }
                </div>
                </React.Fragment>
              }
            </div>
            <div className="submit-button">
              <button onClick={this.addAgenda}>Tambah</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AgendaModal;
