import React, { Component } from 'react';
import { DarkenStyle } from './style';

class DarkenBackground extends Component {
  render() {
    return (
        <DarkenStyle>
            <div className='darken-bg' onClick={this.props.menuToggler}></div>
        </DarkenStyle>
    );
  }
}

export default DarkenBackground;
