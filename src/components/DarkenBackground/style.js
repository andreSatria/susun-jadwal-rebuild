import styled from 'styled-components';

export const DarkenStyle = styled.div`

.darken-bg{
    width:100%;
    height:100%;
    top:0;
    left:0;
    background:black;
    opacity:0.5;
    z-index:100;
    position:fixed;
}


`
