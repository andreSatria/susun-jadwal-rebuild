import styled from 'styled-components';
import DELETE from "./../../assets/buttons/delete.png";

export const CardKelasStyle = styled.div`

    margin:0;

    .kelasCard{
        width:100%;
        height:50px;
        margin:0;
        background:${props => props.theme.colors.lightGrey};
        display:flex;
        margin-bottom:1rem;
        border-radius:0.25rem;

        .kelasName{
            width:35%;
            display:flex;
            justify-content:flex-start;
            align-items:center;

            p{
                margin:0rem 0.15rem 0rem 0.25rem;
                font-size:0.85rem;
            }
        }

        .kelasTime{
            width:45%;
            display:flex;
            flex-direction:column;
            justify-content:center;

            p{
                margin:0;
                font-size:0.85rem;
            }
        }

        .kelasSks{
            width:20%;
            display:flex;
            flex-direction:row;
            justify-content:center;
            align-items:flex-start;

            p{
                margin-top:0.5rem;
                font-size:0.85rem;
            }

        }

        .delete{
            width:30px;
            height:30px;
            margin:-0.75rem -0.75rem 0 0;
            background:url(${DELETE}) no-repeat;
            background-position:center;
            background-size:contain;
            cursor:pointer;
        }




    }

    .kelasCard.valid{
        background:white;

        .delete{
            display:none;
        }
    }
`
