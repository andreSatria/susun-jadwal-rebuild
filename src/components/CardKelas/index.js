import React, { Component } from 'react';
import { CardKelasStyle } from './style';

class CardKelas extends Component {
  render() {
    return (
        <CardKelasStyle>
            <div className='kelasCard' style={{ backgroundColor: this.props.conflict ? null : 'white' }}>
                <div className="kelasName">
                    <p>{this.props.name}</p>
                </div>
                <div className="kelasTime">
                    <p>{this.props.day1},{this.props.start1}-{this.props.end1}</p>
                    <p style={{display: this.props.day2 == null ? "none" : "block" }}>{this.props.day2},{this.props.start2}-{this.props.end2}</p>
                    <p style={{display: this.props.day3 == null ? "none" : "block" }}>{this.props.day3},{this.props.start3}-{this.props.end3}</p>
                </div>
                <div className="kelasSks">
                    <p>{this.props.sks} SKS</p>
                </div>
                <div className="delete" onClick={()=>this.props.remove(this.props.all)}></div>
            </div>
        </CardKelasStyle>
    );
  }
}

export default CardKelas;
