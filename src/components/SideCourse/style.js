import styled from 'styled-components';

export const SideCourseItemContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin:10px 0px;
  padding:5px;
  border-radius:0.5em;
  cursor:pointer;
  ${props => props.isActive && `
    background: ${props.theme.colors.yellow};
    box-shadow: ${props.theme.border.shadowConfig} ${props.theme.border.shadowColor};
  `}
  ${props => !props.isActive && props.empty && `
    background: ${props.theme.colors.lightGrey};
  `}
  /* margin: 12px 0; */

  .titleContainer {
    width: 100%;
    display: flex;
    align-items: center;
    margin: 0 0 10px;
    flex-direction: row;
  }

  .title {
    font-weight: 700;
    font-size: 20px;
    padding:0;
    color: ${props => props.theme.colors.black};
  }

  .desc {
    font-size: 12px;
    margin: 0 8px;
    color: ${props => props.theme.colors.black};
  }

  @media (max-width:768px) {
    .title {
      font-size: 16px;
    }

    .desc{
        font-size:8px;
    }
  }
`;
