import React from 'react';
import PropTypes from 'prop-types';
import { SideCourseItemContainer } from './style';
import SideClassItem from 'components/SideClass';

class SideCourseItem extends React.Component {

  state = {
      toggled : false,

  }

  componentDidMount() {
    this.props.sideDecider;
    const { name, sks, term, classes, pickClass, addedClass,pickedCourses } = this.props;

    if (!this.props.sideDecider) {
        for (let i=0;i<classes.length;i++) {
            for (let j =0;j< pickedCourses.length;j++) {
                if (classes[i].name === pickedCourses[j].name) {
                    pickClass({
                        class: classes[i],
                        course: name,
                        sks,
                    })
                }
            }
        }
    }
  }


  classToggler = () =>{
      return this.setState(prevState => ({
            toggled: !prevState.toggled
        }));
  }

  checker = () => {
     for (let i = 0; i < this.props.data.length; i++) {
         if (this.props.data[i].course === this.props.name) {
             return true;
         }
    }
    return false;
  }

  conflictChecker = () =>{
      for (let i = 0; i < this.props.data.length;i++) {
          if (this.props.name === this.props.data[i].course) {
            return this.props.conflict[i];
          }
      }
      return false;
  }


  deletor = (x,y) => {
      for (let i=0;i<x.length;i++) {
          if (x[i].course === y) return x[i]
      }
      return null
  }

  removeOrAdd = (x,y) => {
      if (x === undefined) return true

      for (let i=0;i<x.length;i++){
          if(x[i].class.name === y) return false
      }
      return true
  }

  render() {
    const { name, sks, term, classes, pickClass, addedClass, pickedCourses } = this.props;
    let activeClass = null;
    // let selected = this.props.data.find(a =>a.includes(name))
    if (addedClass) {

      activeClass = addedClass.class.name;
    }


    return (
      <SideCourseItemContainer isActive={!this.conflictChecker() && this.checker()} empty={this.checker()}>
        <div className="titleContainer" onClick={this.classToggler}>
          <div className="title">
            {name}
          </div>
          <div className="desc">
            ({sks} SKS, Term {term})
          </div>
        </div>
        {this.props.sideDecider ?
            <React.Fragment>
            { this.state.toggled ?
                <React.Fragment>
                    {classes.map((item, index) =>
                    <button key={index}
                    onClick={ this.removeOrAdd(this.props.data,item.name) ? () => pickClass({
                        class: item,
                        course: name,
                        sks,
                    })
                    :
                    ()=> this.props.remove(this.deletor(this.props.data,name))

                    }
                    >
                    <SideClassItem
                    name={item.name}
                    jadwal={item.jadwal}
                    isActive={item.name === activeClass}
                    />
                    </button>
                )}
                </React.Fragment>

                :
                null
            }
            </React.Fragment>
        :
        <React.Fragment>
        { this.state.toggled ?
            <React.Fragment>
                {classes.map((item, index) =>
                <button key={index}
                onClick={ this.removeOrAdd(this.props.data,item.name) ? () => pickClass({
                    class: item,
                    course: name,
                    sks,
                })
                :
                () => this.props.remove(this.deletor(this.props.data,name))

                }
                >
                <SideClassItem
                name={item.name}
                jadwal={item.jadwal}
                isActive={ item.name === activeClass}
                />
                </button>
            )}
            </React.Fragment>

            :
            null
        }
        </React.Fragment>

    }

      </SideCourseItemContainer>
    );
  }
}
SideCourseItem.propTypes = {
  'addedClass': PropTypes.shape().isRequired,
  'classes': PropTypes.shape().isRequired,
  'name': PropTypes.string.isRequired,
  'pickClass': PropTypes.func.isRequired,
  'sks': PropTypes.number.isRequired,
  'term': PropTypes.number.isRequired,
}

export default SideCourseItem;
