import styled from 'styled-components';

export const CourseItemContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin: 12px 0;

  .titleContainer {
    width: 100%;
    display: flex;
    align-items: center;
    margin: 0 0 10px;
    flex-direction: row;
  }

  .title {
    font-weight: 700;
    font-size: 24px;
    padding:0;
    color: ${props => props.theme.colors.black};
  }

  .desc {
    font-size: 12px;
    margin: 6px 8px 0px 8px;
    color: ${props => props.theme.colors.black};
  }

  @media (max-width:768px) {
    .title {
      font-size: 20px;
    }
  }
`;
