import React from 'react';
import PropTypes from 'prop-types';
import { CourseItemContainer } from './style';
import ClassItem from 'components/ClassItem';

class CourseItem extends React.Component {

  render() {
    const { name, sks, term, classes, pickClass, addedClass } = this.props;
    let activeClass = null;
    if (addedClass) {
      activeClass = addedClass.class.name;
    }

    return (
      <CourseItemContainer>
        <div className="titleContainer">
          <div className="title">
            {name}
          </div>
          <div className="desc">
            ({sks} SKS, Term {term})
          </div>
        </div>
        {
          classes.map((item, index) =>
            <button key={index}
              onClick={() => pickClass({
                class: item,
                course: name,
                sks,
              })}
            >
              <ClassItem
                name={item.name}
                lecturer={item.lecturer}
                jadwal={item.jadwal}
                isActive={item.name === activeClass}
              />
            </button>
          )
        }
      </CourseItemContainer>
    );
  }
}

CourseItem.propTypes = {
  'addedClass': PropTypes.shape().isRequired,
  'classes': PropTypes.shape().isRequired,
  'name': PropTypes.string.isRequired,
  'pickClass': PropTypes.func.isRequired,
  'sks': PropTypes.number.isRequired,
  'term': PropTypes.number.isRequired,
}

export default CourseItem;
