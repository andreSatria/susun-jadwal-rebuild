import React from 'react';
import PropTypes from 'prop-types';
import { ClassItemContainer } from './style';

class ClassItem extends React.Component {
  render() {
    const { name, jadwal, lecturer, isActive } = this.props;

    return (
      <ClassItemContainer isActive={isActive}>
        <div className="bulletContainer">
          <div className={isActive
            ? 'bullet bulletActive'
            : 'bullet'}
          />
        </div>
        <div className="nameContainer">
          <div>{name}</div>
        </div>
        <div className="scheduleAndRoom">
          {
            jadwal.map((item, index) =>
            <div key={index} className="schedule">
              <div>{item.day}, {item.start}-{item.end}</div>
              <div>{item.room}</div>
            </div>
            )
          }
        </div>
        <div className="lecturerContainer">
          <div>{lecturer}</div>
        </div>
      </ClassItemContainer>
    );
  }
}

ClassItem.propTypes = {
  'isActive': PropTypes.bool,
  'jadwal': PropTypes.shape().isRequired,
  'lecturer': PropTypes.arrayOf(PropTypes.string),
  'name': PropTypes.string.isRequired,
}

export default ClassItem;
