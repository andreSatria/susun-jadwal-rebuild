import styled from 'styled-components';

export const ClassItemContainer = styled.div`
  width: 100%;
  padding: 8px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  border-radius: ${props => props.theme.border.borderRadius};
  ${props => props.isActive && `
    background: ${props.theme.colors.yellow};
    box-shadow: ${props.theme.border.shadowConfig} ${props.theme.border.shadowColor};
  `}

  .bulletContainer {
    flex: 1;

    .bullet {
      width: 12px;
      height: 12px;
      border-radius: 50%;
      border: 1px solid ${props => props.theme.colors.black};
      background: transparent;
      ${props => props.isActive && `
        background: ${props.theme.colors.black};
      `}
    }

    .bulletActive {
      background: ${props => props.theme.colors.black};
    }
  }

  .nameContainer {
    flex: 2;
  }

  .lecturerContainer {
    flex: 2;
  }

  .scheduleAndRoom {
    flex: 2.5;
    display: flex;
    flex-direction: column;
    justify-content: space-around;

    .schedule {
      width: 80%;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
    }
  }

  @media (max-width:768px) {
    font-size: 12px;

    .bulletContainer {
      flex: 0.5;
    }
    .scheduleAndRoom {
      flex: 4;
    }
  }
`;