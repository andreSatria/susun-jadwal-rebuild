import styled from 'styled-components';
import CROSS from './../../assets/buttons/cross.png';
import KELAS from './../../assets/logos/kelasPilihan.png';

export const SideMenuStyle = styled.div`

    bottom:0%;
    top:0%;


    .sidemenu{
        width:400px;
        min-height:100%;
        background:white;
        box-shadow:2px 0px 5px rgba(0,0,0,0.5);
        position:fixed;
        top:0;
        left:0;
        z-index:200;
        transform:translateX(-100%);
        transition: transform 0.5s ease-out;
        overflow-y: scroll;
        height: 100%;
        padding-bottom:25px;


    }

    .sidemenu::-webkit-scrollbar {
        display: none;
    }


    .sidemenu.open{
        transform:translateX(0);
    }

    /* .wrapper{
        overflow-y: scroll;
        height: 100%;
    } */

    .upper{
        width:100%;
        height:20%;
        display:flex;
        flex-direction:column;

        .cross{
            align-self:flex-start;
            margin:1rem;
            width:30px;
            height:30px;
            background:url(${CROSS}) no-repeat;
            background-size:cover;
            cursor:pointer;
        }

        .container{
            ${props => props.indicator && `
              padding:2rem 0;
            `}
            ${props => !props.indicator && `
              padding:2.5rem 0 0 0;
            `}
            width:100%;
            display:flex;
            flex-direction:row;
            justify-content:space-between;

            .searchButton{
             margin-left:2rem;
              width:2.75rem;
              height:2.7rem;
              background:transparent;
              border:none;

              img{
                width:2.5rem;
                height:auto;
                background-size:contain;

              }
            }
        }

        .option{
                width:auto;
                height:auto;
                align-self:flex-end;
                transform: translateX(-52%);
                border: 0.125rem solid ${props => props.theme.colors.darkBlue};
                line-height:1.25rem;
                border-radius:5rem;
                margin-bottom:0.5rem;

                .default,.live{
                    font-size:10px;
                    font-weight:200;
                    margin-right:1rem;
                    cursor:pointer;

                    &.active{
                        color:white;
                    }

                }

                .default{
                    margin-right:2rem;
                    margin-left:0.5rem;
                }


                .slider{
                    width:60%;
                    height:110%;
                    position:absolute;
                    top:-1px;
                    right:0;
                    bottom:0;
                    left:0;
                    background:${props => props.theme.colors.darkBlue};
                    z-index:-1;
                    border-radius:5rem;
                    transition: transform 0.35s ease-out ;
                    transform: translateX(-2%);
                }

                .slider.on{
                    width:50%;
                    color:white;
                    transform: translateX(101%);
                }

            }

        }

        .searchBar{
          align-self:center;
          width:85%;
          height:2rem;
          border-radius:5em;
          font-size:0.75rem;
          padding: 0.5em 0em 0.5em 1.25em;
          border: solid 2px #cecfcf;
          background:white;
          box-sizing: border-box;
          background: white;
        }


    }

    .mid{
        width:100%;
        height:75%;
        display:flex;
        flex-direction:column;
        justify-content:center;
        align-items:center;


        .editText{
            font-size:1.5rem;
            align-self:flex-start;
            margin-left:2.75rem;
            font-style:bold;
        }

        .kelasLogo{
            width:100%;
            height:150px;
            background:url(${KELAS}) no-repeat;
            background-size:cover;
            background-position:center;
        }

        .content{
            width:85%;
            min-height:100%;
        }

        .jadwal{
            width:67.5%;
            padding:2.5rem 0 2rem 0;

            .edit{
                color:${props => props.theme.colors.yellow};
            }

            .jadwal{
                color:${props => props.theme.colors.blue};
            }
        }

        .scheduleContainer{
            width:80%;
            height:auto;
            min-height:200px;
            max-height:500px;
            overflow-y:auto;
        }

        .centerize{
            margin-top:2rem;
            width:100%;
            height:100%;
            display:flex;
            justify-content:center;
            align-items:center;
            flex-direction:column;

            h3,h5{
                padding:0;
                margin: 0 0 0.5rem 0;
                align-self:center;
                color:${props => props.theme.colors.darkBlue};
            }

            i{
                color:${props => props.theme.colors.blue};

            }
        }

        .longCont{
            width:85%;
            height:24px;
            background:${props => props.theme.colors.darkBlue};
            border-radius:0.25rem;
            margin-bottom:0.5rem;
            display:flex;
            justify-content:flex-end;
            align-items:center;
            cursor:pointer;

            p{
                font-size:0.85rem;
                color:white;
                margin:2.7rem;
            }

            span{
                margin-right:2.25rem;
            }

        }

    }

    .bottom{
        width:100%;
        height:40%;
        display:flex;
        flex-direction:column;
        justify-content:center;
        align-items:center;

        .cont{
            width:85%;
        }

        .conflictText{
            .p{
                color:black;
            }
        }

        .buttons{
            display:flex;
            flex-direction:row;
            justify-content:space-between;
            height:100%;
            margin-bottom:1rem;

            .box{
                display:flex;
                align-items:center;
                justify-content:center;
                align-text:center;
                width:45%;
                height:100px;
                background:${props => props.theme.colors.yellow};
                border-radius:0.5rem;
                box-shadow:3px 8px 6px 0px rgba(0,0,0,0.1);
                transition: all 0.3s ease 0s;
                cursor:pointer;

                p{
                    font-size:1.15rem;
                }

                &:hover{
                  box-shadow: 3px 6px 5px 0px rgba(0,0,0,0.5);
                  transform: translateY(-7px);
                }
            }

            .simpanDisabled{
                background:${props => props.theme.colors.lightGrey};
                pointer-events:none;
            }

        }

        .other{
            .simpan,.simpanDisabled{
                margin-top:12px;
            }

            .simpanDisabled{
                background:${props => props.theme.colors.lightGrey};
                pointer-events:none;
            }
        }

        .longCont{
            width:100%;
            height:24px;
            background:${props => props.theme.colors.yellow};
            border-radius:0.25rem;
            margin-bottom:0.5rem;
            display:flex;
            justify-content:center;
            align-items:center;
            cursor:pointer;

            p{
                font-size:0.85rem;
            }

        }

    }

    @media(max-width:728px){
        .sidemenu{
            width:100%;
        }

        .conflictText{
            font-size:12px;
        }
    }


`
