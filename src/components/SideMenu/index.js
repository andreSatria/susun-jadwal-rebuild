/* eslint-disable complexity, react/jsx-key, object-property-newline, template-curly-spacing */

import React, { Component } from 'react';
import { SideMenuStyle } from './style';
import CardKelas from 'components/CardKelas';
import SideCourseItem from 'components/SideCourse';
import SEARCH from './../../assets/buttons/searchicon(sidebar).png';

class SideMenu extends Component {

    state = {
        pressed: false,
    }

    searchToggler = () => {
        return this.setState((prev) => {
            return { pressed: !prev.pressed };
        });
    }

    searchFunction = (matkul) => {
        if (!matkul) return this.props.course;

        let query = !this.props.query ? "" : this.props.query;
        query = query.replace(/\\/g, "\\\\");
        const filteredCourse = matkul.slice().filter((matkulName) => {
            return matkulName.name.toLowerCase().match(query.toLowerCase());
        })

        return filteredCourse;
    }

    onClickAddAgenda = () => {
        this.props.onClickAddAgenda(true);
    }

    render() {
        const menuClass = ['sidemenu'];

        const liveMode = ['live'];
        const defaultMode = ['default'];
        const slider = ['slider'];

        if (this.props.show) {
            menuClass.push('open');
        }

        if (this.props.indicator) {
            slider.push('on');
            liveMode.push('active');
        } else {
            defaultMode.push('active');
        }

        let filtered = this.searchFunction(this.props.courses)

        const data = this.props.data;

        return (
            <SideMenuStyle indicator={this.props.indicator}>
                <div className={menuClass.join(' ')}>
                    <div className="wrapper">
                        <div className="upper">
                            <div className="cross" onClick={this.props.menuToggler} ></div>
                            <div className="container">
                                <div className="image">
                                { this.props.indicator ?
                                    <React.Fragment>
                                        <button className="searchButton" onClick={this.searchToggler}>
                                            <img src={SEARCH} alt="search" />
                                        </button>
                                    </React.Fragment>

                                    :

                                    null

                                }
                                </div>
                                <div className="option">
                                    <span className={defaultMode.join(' ')} onClick={this.props.defaultToggler}>Default</span>
                                    <span className={liveMode.join(' ')} onClick={this.props.liveToggler}>Live</span>
                                    <div className={slider.join(' ')}></div>
                                </div>
                            </div>
                            {this.props.indicator ?
                                <input className="searchBar" type="text" placeholder="Cari Mata Kuliah" style={{display: !this.state.pressed ? 'none': 'block'}} onChange={(e) => this.props.search(e.target.value)}></input>
                                :
                                null
                            }
                        </div>
                        <div className="mid">
                        { this.props.sideDecider ?
                            <React.Fragment>
                            {this.props.indicator ?
                                <React.Fragment>
                                    <h1 className="editText"><b>Edit Jadwal</b></h1>
                                    <div className="scheduleContainer">
                                      { filtered.length !== 0 ?
                                        filtered.map((item, index) =>
                                          <SideCourseItem
                                            key={index}
                                            name={item.name}
                                            sks={item.sks}
                                            term={item.term}
                                            classes={item.class}
                                            pickClass={this.props.pickClass}
                                            data = {this.props.data}
                                            conflict = {this.props.decider}
                                            remove = {this.props.remove}
                                            pickedCourses={this.props.pickedCourses}
                                            sideDecider={this.props.sideDecider}
                                            addedClass={this.props.data.find((cl) => cl.course === item.name)}
                                          />
                                        )
                                        :
                                        <React.Fragment>
                                            <div className="centerize">
                                                <div><h3>Whoops!</h3></div>
                                                <div><h5>The Course <i>"{this.props.query}"</i> is unavailable</h5></div>
                                            </div>
                                        </React.Fragment>
                                      }
                                    </div>
                                </React.Fragment>
                                :
                                <React.Fragment>
                                <div className="kelasLogo"></div>
                                <div className="content">
                                    {data.map((kelas, index) => {
                                        return kelas.class.jadwal.length === 2 ?
                                        <CardKelas
                                            key={index}
                                            kelas ={kelas.course}
                                            name ={kelas.class.name}
                                            day1={kelas.class.jadwal[0].day}
                                            start1={kelas.class.jadwal[0].start}
                                            end1={kelas.class.jadwal[0].end}
                                            day2={kelas.class.jadwal[1].day}
                                            start2={kelas.class.jadwal[1].start}
                                            end2={kelas.class.jadwal[1].end}
                                            sks={kelas.sks}
                                            remove={this.props.remove}
                                            all={kelas}
                                            conflict={this.props.decider[index]}
                                        />
                                        : kelas.class.jadwal.length === 1 ?
                                        <CardKelas
                                            key={index}
                                            kelas ={kelas.course}
                                            name ={kelas.class.name}
                                            day1={kelas.class.jadwal[0].day}
                                            start1={kelas.class.jadwal[0].start}
                                            end1={kelas.class.jadwal[0].end}
                                            day2={null}
                                            start2={null}
                                            end2={null}
                                            sks={kelas.sks}
                                            remove={this.props.remove}
                                            all={kelas}
                                            conflict={this.props.decider[index]}
                                        />
                                        :
                                        <CardKelas
                                            key={index}
                                            kelas ={kelas.course}
                                            name ={kelas.class.name}
                                            day1={kelas.class.jadwal[0].day}
                                            start1={kelas.class.jadwal[0].start}
                                            end1={kelas.class.jadwal[0].end}
                                            day2={kelas.class.jadwal[1].day}
                                            start2={kelas.class.jadwal[1].start}
                                            end2={kelas.class.jadwal[1].end}
                                            day3={kelas.class.jadwal[2].day}
                                            start3={kelas.class.jadwal[2].start}
                                            end3={kelas.class.jadwal[2].end}
                                            sks={kelas.sks}
                                            remove={this.props.remove}
                                            all={kelas}
                                            conflict={this.props.decider[index]}
                                        />
                                    })

                                }
                                </div>
                                <div className="count longCont"><p><span>Total SKS</span>{this.props.count}</p></div>
                                </React.Fragment>
                            }
                            </React.Fragment>
                            :
                            <React.Fragment>
                            {this.props.indicator ?
                                <React.Fragment>
                                    <h1 className="editText"><b>Edit Jadwal</b></h1>
                                    <div className="scheduleContainer">
                                      { filtered.length !== 0 ?
                                        filtered.map((item, index) =>
                                          <SideCourseItem
                                            key={index}
                                            name={item.name}
                                            sks={item.sks}
                                            term={item.term}
                                            classes={item.class}
                                            pickClass={this.props.pickClass}
                                            data = {this.props.data}
                                            conflict = {this.props.decider}
                                            remove = {this.props.remove}
                                            pickedCourses={this.props.pickedCourses}
                                            sideDecider={this.props.sideDecider}
                                            addedClass={this.props.data.find((cl) => cl.course === item.name)}
                                          />
                                        )
                                        :
                                        <React.Fragment>
                                            <div className="centerize">
                                                <div><h3>Whoops!</h3></div>
                                                <div><h5>The Course <i>"{this.props.query}"</i> is unavailable</h5></div>
                                            </div>
                                        </React.Fragment>
                                      }
                                    </div>
                                </React.Fragment>
                                :
                                <React.Fragment>
                                <div className="content jadwal">
                                    <h2><b>Halo!</b></h2>
                                    {
                                        this.props.pickedCourses && (
                                            this.props.pickedCourses.length > 0 ?
                                                <div>
                                                    <span>Berikut ini adalah jadwal yang sudah kamu buat. Kamu masih bisa mengubah jadwal tersebut dengan menekan <b className="edit">Edit</b> <b className="jadwal">Jadwal</b></span>

                                                    {/* {
                                                        !this.props.indicate ?
                                                        <div>
                                                            <br/> <br/>
                                                            <span>Kamu juga bisa menambahkan jadwal yang sudah dibuat ke Google Calendar lho! Kamu bisa melakukan itu dengan menekan <b style={{color:'#dd4b39'}}>Connect Google Calendar</b></span>
                                                        </div>
                                                        :
                                                        null
                                                    } */}
                                                </div>
                                            :
                                                <span>Saat ini jadwal kamu masih kosong. Silahkan menambahkan jadwal dengan menekan <b className="edit">Edit</b> <b className="jadwal">Jadwal</b></span>

                                        )
                                    }
                                </div>
                                </React.Fragment>
                            }
                            </React.Fragment>
                        }
                        </div>
                    <div className="bottom">
                        { this.props.sideDecider ?
                            <React.Fragment>
                                <div className="conflictText cont">
                                    {this.props.indicate ? <p><b>Halo!</b><br/>Terdapat <b>konflik</b> pada susunan jadwal Anda <br/> Silahkan diperbaiki</p> : null }
                                </div>
                                {this.props.indicator ?
                                    <div className="other cont">
                                        {!this.props.indicate && this.props.decider.length !== 0 ? <div className="simpan longCont" onClick={() => this.props.onClickSimpanJadwal(this.props.data)}><p>Simpan Jadwal</p></div> : <div className="simpanDisabled longCont"><p>Simpan Jadwal</p></div>}
                                        <div className="logout longCont"><p>Logout</p></div>
                                    </div>
                                    :
                                    <React.Fragment>
                                        <div className="buttons cont">
                                            <div className="tambah box" onClick={this.onClickAddAgenda}><p><b>Tambah</b><br/>Agenda</p></div>
                                            {!this.props.indicate && this.props.decider.length !== 0 ? <div className="simpan box" onClick={() => this.props.onClickSimpanJadwal(this.props.data)}><p><b>Simpan</b><br/> Jadwal</p></div> : <div className="simpanDisabled box"><p><b>Simpan</b><br/> Jadwal</p></div>}
                                        </div>
                                        <div className="other cont">
                                            <div className="lihat longCont" onClick={() => this.props.onClickPindahPage('/jadwal')}><p>Lihat Jadwal Saya</p></div>
                                            <div className="logout longCont" onClick={() => this.props.onClickPindahPage('/logout')}><p>Logout</p></div>
                                        </div>
                                    </React.Fragment>
                                }
                                </React.Fragment>
                                :
                                <React.Fragment>
                                {this.props.indicator ?
                                    <React.Fragment>
                                    <div className="conflictText cont">
                                        {this.props.indicate ? <p><b>Halo!</b><br/>Terdapat <b>konflik</b> pada susunan jadwal Anda <br/> Silahkan diperbaiki</p> : null }
                                    </div>
                                    <div className="other cont">
                                        {!this.props.indicate && this.props.decider.length !== 0 ? <div className="simpan longCont" onClick={() => this.props.onClickSimpanJadwal(this.props.data, true)}><p>Simpan Jadwal</p></div> : <div className="simpanDisabled longCont"><p>Simpan Jadwal</p></div>}
                                        <div className="logout longCont"><p>Logout</p></div>
                                    </div>
                                    </React.Fragment>
                                    :
                                    <React.Fragment>
                                    <div className="other cont">
                                        <div className="simpan longCont" onClick={() => this.props.onClickPindahPage('/susun')}><p>Edit Jadwal</p></div>
                                        {/* {
                                            this.props.pickedCourses && (this.props.pickedCourses.length > 0 && !this.props.indicate ?
                                                <div className="simpan longCont" style={{background: '#dd4b39', color: 'white'}} onClick={() => this.props.onClickGoogleCalendar()}><p>Connect Google Calendar</p></div>
                                            :
                                            null
                                            )
                                        } */}
                                        <div className="logout longCont" onClick={() => this.props.onClickPindahPage('/logout')}><p>Logout</p></div>

                                    </div>
                                    </React.Fragment>
                                }
                                </React.Fragment>
                            }
                        </div>
                    </div>
                </div>
            </SideMenuStyle>
        );
    }
}

export default SideMenu;
