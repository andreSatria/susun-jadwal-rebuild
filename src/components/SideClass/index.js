import React from 'react';
import PropTypes from 'prop-types';
import { SideClassItemContainer } from './style';

class SideClassItem extends React.Component {

  render() {

    const { name, jadwal, lecturer, isActive } = this.props;

    return (
      <SideClassItemContainer>
        <div className="bulletContainer">
          <div className={isActive
            ? 'bullet bulletActive'
            : 'bullet'}
          />
        </div>
        <div className="nameContainer">
          <div>{name}</div>
        </div>
        <div className="scheduleAndRoom">
          {
            jadwal.map((item, index) =>
            <div key={index} className="schedule">
              <div>{item.day}, {item.start}-{item.end}</div>
            </div>
            )
          }
        </div>
      </SideClassItemContainer>
    );
  }
}

SideClassItem.propTypes = {
  'isActive': PropTypes.bool,
  'jadwal': PropTypes.shape().isRequired,
  'lecturer': PropTypes.arrayOf(PropTypes.string),
  'name': PropTypes.string.isRequired,
}

export default SideClassItem;
