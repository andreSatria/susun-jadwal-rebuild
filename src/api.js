let API_PREFIX_URL = 'http://127.0.0.1:5000';

const BASE_PATH = '/susunjadwal/api'

if (process.env.NODE_ENV === 'production') {
    API_PREFIX_URL = 'ristek.cs.ui.ac.id';
  }

const API_PREFIX_LOGIN = API_PREFIX_URL;
API_PREFIX_URL += BASE_PATH;

// Method: POST
export const authLoginApi = `${API_PREFIX_LOGIN}/auth/login`;
// Method: GET
export const fetchCoursesApi = (majorId) => `${API_PREFIX_URL}/majors/${majorId}/courses`;

// Method: POST (PickSchedulePage and NavigationMenu)
export const saveJadwalApi = (userId) => `${API_PREFIX_URL}/users/${userId}/jadwals`;
export const setJadwalUtamaApi = (userId, jadwalId) => `${API_PREFIX_URL}/users/${userId}/jadwals/${jadwalId}/set-utama`;

// Method: GET (ShowSchedulePage)
export const fetchJadwalUserApi = (userId) => `${API_PREFIX_URL}/users/${userId}/jadwals`;
export const fetchJadwalApi = (jadwalId) => `${API_PREFIX_URL}/jadwals/${jadwalId}`;

// Method: POST
export const addGoogleCalendarApi = (userId, jadwalId) => `${API_PREFIX_URL}/users/${userId}/jadwals/${jadwalId}/add-to-calendar`;


// export const fetchCoursesApi = 'https://5c28ca91dc7d0a00144c2e50.mockapi.io/susun';
// export const fetchPickedCoursesApi = 'http://5c27bf434977c20014878ee7.mockapi.io/api/my-courses/1';
// export const submitPickedCoursesApi = 'https://5c27bf434977c20014878ee7.mockapi.io/api/my-courses/';
