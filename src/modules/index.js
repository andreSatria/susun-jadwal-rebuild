/* eslint-disable */
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import pickSchedulePageReducer from 'containers/PickSchedulePage/reducer';
import navReducer from 'containers/NavigationMenu/reducer';
import showSchedulePageReducer from 'containers/ShowSchedulePage/reducer';
import loginPageReducer from 'containers/LoginPage/reducer';

export default combineReducers({
  'routing': routerReducer,
  'pickSchedulePageReducer': pickSchedulePageReducer,
  'navReducer': navReducer,
  'showSchedulePageReducer': showSchedulePageReducer,
  'loginPageReducer': loginPageReducer,
});
