import { getCookie } from 'utils';

export const getPickSchedulePageReducer = (state) => state.pickSchedulePageReducer.toJS();
export const getCourses = (state) => getPickSchedulePageReducer(state).courses;
export const isLoadingCourses = (state) => getPickSchedulePageReducer(state).isLoading;
export const isLoadedCourses = (state) => getPickSchedulePageReducer(state).isLoaded;
export const getAddedClasses = (state) => getPickSchedulePageReducer(state).addedClasses;

export const getShowSchedulePageReducer = (state) => state.showSchedulePageReducer.toJS();
export const getPickedCourses = (state) => getShowSchedulePageReducer(state).pickedCourses;
export const getActiveJadwal = (state) => getShowSchedulePageReducer(state).activeJadwal;
export const isLoadingPickedCourses = (state) => getShowSchedulePageReducer(state).isLoading;
export const isLoadedPickedCourses = (state) => getShowSchedulePageReducer(state).isLoaded;


export const getAuthUser = () => {
    const major_id = getCookie("major_id");
    const token = getCookie("token");
    const user_id = getCookie("user_id");

    return {
        major_id,
        token,
        user_id,
    }
}

export const getUser = (state) => state.loginPageReducer.toJS();

export const getNavReducer = (state) => state.navReducer.toJS();
export const getQuery = (state) => getNavReducer(state).query;
export const getMenu = (state) => getNavReducer(state).menuDecider;
